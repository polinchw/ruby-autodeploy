require 'webrick'

server = WEBrick::HTTPServer.new Port: 5000

# The following proc is used to customize the server operations
server.mount_proc '/' do |_request, response|
  response.body = '<h1>This should be merged to the master.</h1>'
end

server.start
